# CroqueMotel Contributing Guide

## Development

### Yarn scripts

This scripts can be run inside `app` folder:

```sh
# Launch the dev server locally
yarn run dev

# Build all app source code (to dist folder)
yarn run build

# Launch the server from the builded code
yarn run start

# ESLint validation
yarn run lint

# Run unit tests on app/server
yarn run test:server

# Run unit and e2e tests to app/client
yarn run test:client
```

### Project structure

- `.docker`: contains Dockerfiles and setup scripts for Docker containers.
- `.vscode`: contains VSCode preconfigured debugger settings.
- `app`: contains all application related code.
    - `client`: contains Nuxt client-side source code.
        - Sub-folders follows the [Nuxt Directory Structure](https://nuxtjs.org/docs/2.x/get-started/directory-structure).
    - `dist`: contains all generated files after a build action.
    - `server`: contains server-side source code.
        - `migrations`: contains TypeORM migrations.
        - `modules`: contains handlers and services grouped by feature.
            - `<module>/dtos`: contains Data Transfert Objects used in API replies.
            - `<module>/entities`: contains data entities (saved in database).
            - `<module>/enums`: contains enums (static) data.
            - `<module>/handlers`: contains REST API handlers.
        - `plugins`: contains Fastify plugins.
        - `services`: contains common app services.
- `etwin`: contains ETwin website dependency, used for OAuth and ETwin API.

### Debugging using Visual Studio Code

The `.vscode/launch.json` for debugging the `app` project includes 2 configs:

- **Attach App (docker):** Attach the debugger to the app, you must start docker containers first.
- **Launch App (dev):** Launch the app in legacy mode with live-reload.
