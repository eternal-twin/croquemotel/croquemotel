export interface RoomBuildConfig {
  delay: number;
  price: number | ((count: number) => number);
  max?: number;
  fame?: number;
  requiredHotelLevel: number;
  disallowFloor0?: boolean;
}
