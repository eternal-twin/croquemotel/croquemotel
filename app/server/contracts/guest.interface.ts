import { GuestLikes } from "./guest-likes.enum";
import { GuestType } from "./guest-type.enum";

export interface IGuest {
  id: number;
  hotelId: string;
  type: GuestType;
  name: string;
  likes: GuestLikes;
  dislikes: GuestLikes;
  effects: GuestLikes;
  isVip: boolean;
  createdAt: Date;
  updatedAt: Date;
}
