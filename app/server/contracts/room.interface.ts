import { BedRoomLevel } from "./bedroom-level.enum";
import { IGuest } from "./guest.interface";
import { LobbyRoomLevel } from "./lobbyroom-level.enum";
import { RoomEquipment } from "./room-equipment.enum";
import { ServiceRoomType } from "./serviceroom-type.enum";
import { SpecialRoomType } from "./specialroom-type.enum";
import { IStaff } from "./staff.interface";

export type RoomType = "bed" | "empty" | "service" | "special" | "lobby";

export interface IRoom {
  id?: number;
  type: RoomType;
  floorId: number;
  position: number;
  staffs?: IStaff[];
  buildingEndAt?: Date;
  createdAt: Date;
  updatedAt: Date;
}

export interface IEmptyRoom extends IRoom {

}

export interface IBedRoom extends IRoom {
  level: BedRoomLevel;
  damages: number;
  guest?: IGuest;
  equipment1?: RoomEquipment;
  equipment2?: RoomEquipment;
  serviceEndAt?: Date;
}

export interface ISpecialRoom extends IRoom {
  service: SpecialRoomType;
  serviceEndAt?: Date;
  guest?: IGuest;
  progress?: number;
}

export interface IServiceRoom extends IRoom {
  service: ServiceRoomType;
  serviceEndAt?: Date;
  guest?: IGuest;
}

export interface ILobbyRoom extends IRoom {
  level: LobbyRoomLevel;
}
