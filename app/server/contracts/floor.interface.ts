import { IRoom } from "./room.interface";

export interface IFloor {
  id?: number;
  hotelId: number;
  rooms?: IRoom[];
  level: number;
  paint: number;
  wallpaper: number;
  wallpaperTilt: number;
  lowerWall?: number;
  createdAt: Date;
  updatedAt: Date;
}
