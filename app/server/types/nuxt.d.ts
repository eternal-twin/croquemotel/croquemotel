declare module "nuxt" {
  import { NuxtApp } from "@nuxt/types/app";
  import { IncomingMessage, ServerResponse } from "node:http";

  type LoadOptions = {
    for: LoadOptionsFor,
  };

  type LoadOptionsFor = "dry" | "dev" | "build" | "start";

  const loadNuxt: (options?: LoadOptions | LoadOptionsFor) => Promise<NuxtInstance>;

  const build: (nuxt: NuxtInstance) => void;

  type NuxtRenderRouteContext = { req?: any, res?: any };

  interface NuxtInstance extends NuxtApp {
    close: () => void;
    render: (req: IncomingMessage, res: ServerResponse) => Promise<void>;
    renderRoute: (route: string, context?: NuxtRenderRouteContext) => Promise<{
      html: string,
      error?: {
        statusCode: number,
        message: string
      },
      redirected: boolean | Object
    }>
  }
}
