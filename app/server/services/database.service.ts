import { Destructor, Initializer, Service } from "fastify-decorators";
import { Connection, createConnection } from "typeorm";
import { BetterSqlite3ConnectionOptions } from "typeorm/driver/better-sqlite3/BetterSqlite3ConnectionOptions";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";

import * as entities from "../modules/entities";

@Service()
export class DatabaseService {
  private _connection!: Connection;

  public get connection(): Connection {
    return this._connection;
  }

  @Initializer()
  public async init(): Promise<void> {
    const connectionOptions = {
      ...this.getConnectionOptions(),
      synchronize: process.dev,
      logging: process.dev,
      entities: [
        // resolve(dirname(fileURLToPath(import.meta.url)), "../modules/**/*.entity{.ts,.js}"),
        ...Object.values(entities)
      ]
      // migrations: [
      //   path.resolve(__dirname, "migrations/**/*{.ts,.js}")
      // ]
    };

    this._connection = await createConnection(connectionOptions);
  }

  @Destructor()
  public async destroy(): Promise<void> {
    await this._connection.close();
  }

  private getConnectionOptions(): BetterSqlite3ConnectionOptions | PostgresConnectionOptions {
    if (process.env.DB_ENGINE === "sqlite") {
      return {
        type: "better-sqlite3",
        database: ":memory:"
      };
    } else {
      return {
        type: "postgres",
        host: process.env.DB_HOST!,
        port: 5432, // parseInt(process.env.DB_PORT!),
        username: process.env.DB_USER!,
        password: process.env.DB_PASS!,
        database: process.env.DB_NAME_APP!
      };
    }
  }
}
