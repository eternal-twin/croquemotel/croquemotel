import { Initializer, Service } from "fastify-decorators";
import { EntityTarget, Repository } from "typeorm";

import { BedRoomLevel } from "../../contracts/bedroom-level.enum";
import { RoomBuildConfig } from "../../contracts/room-build-config.interface";
import { ServiceRoomType } from "../../contracts/serviceroom-type.enum";
import { SpecialRoomType } from "../../contracts/specialroom-type.enum";
import { DatabaseService } from "../../services/database.service";
import { BaseService, ServiceResult } from "../../services/service";
import { Floor } from "./entities/floor.entity";
import { Hotel } from "./entities/hotel.entity";
import { BedRoom, EmptyRoom, Room, ServiceRoom, SpecialRoom } from "./entities/room.entity";
import { HotelService } from "./hotel.service";

@Service()
export class BuildService extends BaseService {
  private hotelRepository!: Repository<Hotel>;
  private floorRepository!: Repository<Floor>;
  private roomRepository!: Repository<Room>;

  private static extensionsPrices = {
    9: [0, 0, 0, 0],
    12: [50, 50, 50, 40],
    15: [250, 240, 220, 200],
    17: [500, 470, 440, 400],
    19: [1000, 940, 870, 800],
    _: [100, 94, 87, 80]
  };

  constructor(
    private db: DatabaseService,
    private hotelService: HotelService
  ) {
    super();
  }

  @Initializer([DatabaseService])
  init() {
    this.hotelRepository = this.db.connection.getRepository(Hotel);
    this.floorRepository = this.db.connection.getRepository(Floor);
    this.roomRepository = this.db.connection.getRepository(Room);
  }

  /**
   * Gets the total number of rooms in a hotel.
   * @param hotelId Target hotel ID.
   * @param type The room type to count.
   * @returns The number of rooms in the hotel.
   */
  public async getRoomsCount<T extends Room>(hotelId: number, type: EntityTarget<T> = Room): Promise<number> {
    return await this.db.connection.getRepository(type)
      .createQueryBuilder("r")
      .leftJoin("floor", "f", "f.id = r.floorId")
      .where("f.hotelId = :hotelId", { hotelId })
      .getCount();
  }

  /**
   * Calculate the price of a new empty room in the hotel.
   * @param hotelId Target hotel ID.
   * @returns The price of new room in the hotel.
   */
  public async getNewExtensionPrice(hotelId: number): Promise<number> {
    const roomsCount = await this.getRoomsCount(hotelId);

    let roomPriceIndex = "_";
    for (const key of Object.keys(BuildService.extensionsPrices)) {
      roomPriceIndex = key;

      if (key !== "_" && roomsCount <= Number(key)) {
        break;
      }
    }

    // TODO: Ajouter niveau "architecte fou".
    const discountLevel = 0;

    let roomPrice: number = (BuildService.extensionsPrices as any)[roomPriceIndex][discountLevel];

    if (roomPriceIndex === "_") {
      roomPrice = roomPrice * (roomsCount - 6);
    }

    return roomPrice;
  }

  /**
   * Built a new empty room in the given hotel.
   * @param hotelOrPlayer Hotel to update.
   * @param options Floor level and room position.
   * @returns A successful result with the new EmptyRoom or an erroneous result if the room can't be build.
   */
  public async buildNewExtension(hotelOrPlayer: Hotel | number | string, options: { floorLevel: number, position: number }): Promise<ServiceResult<EmptyRoom>> {
    const hotel = await this.hotelService.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    const extensionPrice = await this.getNewExtensionPrice(hotel.id);

    if (extensionPrice > 0) {
      const canPayResult = await this.hotelService.canPay(hotel, extensionPrice);

      if (!canPayResult.succeeded) {
        return canPayResult.castFailed();
      }

      if (!canPayResult.value) {
        return this.failForbidden();
      }
    }

    const floors = hotel.floors ??
      (await this.floorRepository.find({
        where: { hotelId: hotel.id },
        relations: ["rooms"]
      }));

    if (!floors || floors.length === 0) {
      return this.failNotFound();
    }

    let floor = floors.find(f => f.level === options.floorLevel);

    if (!floor) {
      const lowerFloorLevel = options.floorLevel - 1;
      const lowerFloor = floors.find(f => f.level === lowerFloorLevel);

      if (!lowerFloor) {
        return this.failInvalid();
      }

      floor = new Floor();
      floor.level = options.floorLevel;
      floor.rooms = [];
      floors.push(floor);
    }

    // If we're building a new room on left (new column)
    if (options.position === -1) {
      // If there is not adjacent room in this floor
      if (!floor.rooms!.some(r => r.position === 0)) {
        return this.failInvalid();
      }

      // Update all rooms position
      floors.forEach(f => f.rooms?.forEach(r => r.position++));
      options.position = 0;

    // Else, if it's not a new floor and there is no adjacent room in this floor, or there is already a room at this position
    } else if (
      floor.rooms!.length !== 0 && (
        !floor.rooms!.some(r => r.position + 1 === options.position) ||
        floor.rooms!.some(r => r.position === options.position)
      )
    ) {
      return this.failInvalid();
    }

    // If we're building on right side at floor 0, we move the lobby on the right
    if (options.floorLevel === 0 && options.position === floor.rooms!.length) {
      const lobbyRoom = floor.rooms!.reduce((prev, curr) => curr.position > prev.position ? curr : prev);
      lobbyRoom.position = options.position;
      options.position--;
    }

    const room = new EmptyRoom();
    room.position = options.position;
    floor.rooms!.push(room);
    hotel.floors = floors;

    const payResult = await this.hotelService.pay(hotel, extensionPrice, false);

    if (!payResult.succeeded) {
      return payResult.castFailed();
    }

    try {
      await this.hotelRepository.save(hotel);
    } catch (e: any) {
      return this.fail(e.message);
    }

    return this.succeed(room);
  }

  /**
   * Built a new bed room (in an empty room).
   * @param hotelOrPlayer Hotel to update.
   * @param roomOrId The empty room to convert, or the room to destroy.
   * @returns A successful result with the new BedRoom or an erroneous result if the room can't be build.
   */
  public async buildNewBedRoom(hotelOrPlayer: Hotel | number | string, roomOrId: Room | number): Promise<ServiceResult<BedRoom>> {
    const hotel = await this.hotelService.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    const room = await this.getRoom(hotel, roomOrId);

    if (!room) {
      return this.failInvalid();
    }

    return await this.buildRoom(
      hotel,
      room,
      BedRoom,
      BedRoomLevel.ONE.buildConfig
    );
  }

  /**
   * Upgrade an existing bed room.
   * @param hotelOrPlayer Hotel to update.
   * @param bedRoom The bedroom to upgrade.
   * @returns A successful result with the new BedRoom or an erroneous result if the room can't be upgraded.
   */
  public async upgradeBedRoom(hotelOrPlayer: Hotel | number | string, bedRoom: BedRoom | number): Promise<ServiceResult<BedRoom>> {
    const hotel = await this.hotelService.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    if (!(bedRoom instanceof BedRoom)) {
      return this.failInvalid();
    }

    const bedRoomLevel = BedRoomLevel.from(bedRoom.level.level + 1);

    if (typeof bedRoomLevel === "undefined") {
      return this.failInvalid();
    }

    if (!bedRoom.isBuilt) {
      return this.failInvalid();
    }

    const canPayResult = await this.hotelService.canPay(hotel, bedRoomLevel.buildConfig.price as number);

    if (!canPayResult.succeeded) {
      return canPayResult.castFailed();
    }

    if (!canPayResult.value) {
      return this.failForbidden();
    }

    bedRoom.level = bedRoomLevel;
    bedRoom.buildingEndAt = new Date(Date.now() + bedRoomLevel.buildConfig.delay);

    return await this.db.connection.transaction(async (entityManager) => {
      try {
        await entityManager.save(BedRoom, bedRoom);
        await entityManager.update(Hotel, hotel.id, { money: hotel.money });
        return this.succeed(bedRoom);
      } catch (e: any) {
        return this.fail(e.message);
      }
    });
  }

  /**
   * Built a new service room (in an empty room).
   * @param hotelOrPlayer Hotel to update.
   * @param roomOrId The empty room to convert, or the room to destroy.
   * @param service The service type provided by the room.
   * @returns A successful result with the new ServiceRoom or an erroneous result if the room can't be build.
   */
  public async buildNewServiceRoom(hotelOrPlayer: Hotel | number | string, roomOrId: Room | number, service: string): Promise<ServiceResult<ServiceRoom>> {
    const hotel = await this.hotelService.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    const serviceType = ServiceRoomType.from(service);

    if (typeof serviceType === "undefined") {
      return this.failInvalid();
    }

    const room = await this.getRoom(hotel, roomOrId);

    if (!room) {
      return this.failInvalid();
    }

    return await this.buildRoom(
      hotel,
      room,
      ServiceRoom,
      serviceType.buildConfig,
      (entity) => { entity.service = serviceType; }
    );
  }

  /**
   * Built a new special room (in an empty room).
   * @param hotelOrPlayer Hotel to update.
   * @param roomOrId The empty room to convert, or the room to destroy.
   * @param service The service type provided by the room.
   * @returns A successful result with the new SpecialRoom or an erroneous result if the room can't be build.
   */
  public async buildNewSpecialRoom(hotelOrPlayer: Hotel | number | string, roomOrId: Room | number, service: string): Promise<ServiceResult<SpecialRoom>> {
    const hotel = await this.hotelService.get(hotelOrPlayer);
    if (!hotel) {
      return this.failNotFound();
    }

    const serviceType = SpecialRoomType.from(service);

    if (typeof serviceType === "undefined") {
      return this.failInvalid();
    }

    const room = await this.getRoom(hotel, roomOrId);

    if (!room) {
      return this.failInvalid();
    }

    return await this.buildRoom(
      hotel,
      room,
      SpecialRoom,
      serviceType.buildConfig,
      (entity) => { entity.service = serviceType; }
    );
  }

  /**
   * Gets the room if it belongs to the given hotel.
   * @param hotel The hotel that contain the room.
   * @param roomOrId The room or room ID.
   * @returns The given room (or the one find by ID), or `undefined` if the room is invalid.
   */
  private async getRoom(hotel: Hotel, roomOrId: Room | number): Promise<Room | undefined> {
    const room = roomOrId instanceof Room ? roomOrId : await this.roomRepository.findOne(roomOrId);

    if (!room) {
      return undefined;
    }

    const count = await this.floorRepository.count({ where: { id: room.floorId, hotelId: hotel.id } });
    if (count !== 1) {
      return undefined;
    }

    return room;
  }

  /**
   * Built a new room (in an empty room).
   * @param hotel The hotel to update.
   * @param room The empty room to convert, or the room to destroy.
   * @param type The room entity type to build.
   * @param config Build configs.
   * @param onCreate Called after new entity is created, allow to inject data.
   * @returns A successful result with the new Room or an erroneous result if the room can't be build.
   */
  private async buildRoom<T extends Room>(hotel: Hotel, room: Room, type: EntityTarget<T>, config: RoomBuildConfig, onCreate?: (entity: T) => void): Promise<ServiceResult<T>> {
    if (hotel.level < config.requiredHotelLevel) {
      return this.failForbidden();
    }

    const count = typeof config.max !== "undefined" || typeof config.price !== "number"
      ? await this.getRoomsCount(hotel.id, type)
      : 0;

    if (config.max && count >= config.max) {
      return this.failForbidden();
    }

    const buildingPrice = typeof config.price === "number" ? config.price : config.price(count);

    const canPayResult = await this.hotelService.canPay(hotel, buildingPrice);

    if (!canPayResult.succeeded) {
      return canPayResult.castFailed();
    }

    if (!canPayResult.value) {
      return this.failForbidden();
    }

    const floor = await this.floorRepository.findOne(room.floorId, {
      where: { hotelId: hotel.id }
    });

    if (!floor) {
      return this.failInvalid();
    }

    if (config.disallowFloor0 && floor.level === 0) {
      return this.failInvalid();
    }

    const newRoom = this.db.connection.manager.create(type);
    newRoom.id = room.id;
    newRoom.floorId = room.floorId;
    newRoom.position = room.position;
    newRoom.buildingEndAt = new Date(Date.now() + config.delay);

    if (onCreate) {
      onCreate(newRoom);
    }

    const payResult = await this.hotelService.pay(hotel, buildingPrice, false);

    if (!payResult.succeeded) {
      return payResult.castFailed();
    }

    return await this.db.connection.transaction(async (entityManager) => {
      try {
        await entityManager.delete(Room, room.id);
        await entityManager.insert(type, newRoom as any);
        await entityManager.update(Hotel, hotel.id, { money: hotel.money });
        return this.succeed(newRoom);
      } catch (e: any) {
        return this.fail(e.message);
      }
    });
  }
}
