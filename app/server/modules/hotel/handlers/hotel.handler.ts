import { Static, Type } from "@sinclair/typebox";
import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, GET } from "fastify-decorators";

import { authenticate } from "../../auth/auth.helper";
import { HotelService } from "../hotel.service";

const HotelField = Type.String();
const HotelFields = Type.Object({
  fields: Type.Optional(Type.Array(HotelField))
});
type HotelFields = Static<typeof HotelFields>;

@Controller("/api/hotel")
export default class HotelHandler {
  constructor(
    private hotelService: HotelService
  ) { }

  @GET("/:id", {
    schema: {
      params: {
        id: { type: "number" }
      },
      querystring: HotelFields
    }
  })
  public async get(request: FastifyRequest<{ Params: { id: number }, Querystring: HotelFields }>, reply: FastifyReply) {
    const result = await this.hotelService.findHotel(request.params.id, request.query.fields);
    reply.sendServiceResult(result);
  }

  @GET("", {
    preHandler: authenticate,
    schema: {
      querystring: HotelFields
    }
  })
  public async getCurrent(request: FastifyRequest<{ Querystring: HotelFields }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;
    const result = await this.hotelService.findHotel(hotelId, request.query.fields);
    reply.sendServiceResult(result, { groups: ["player"] });
  }
}
