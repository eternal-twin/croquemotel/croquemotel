import { Static, Type } from "@sinclair/typebox";
import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, POST } from "fastify-decorators";

import { authenticate } from "../../auth/auth.helper";
import { BuildService } from "../build.service";

const BuildExtensionBody = Type.Object({
  floorLevel: Type.Number({ minimum: 0 }),
  position: Type.Number({ minimum: -1 })
});
type BuildExtensionBody = Static<typeof BuildExtensionBody>;

@Controller("/api/hotel/build/extension")
export default class BuildExtensionHandler {
  constructor(
    private buildService: BuildService
  ) { }

  @POST("", {
    preHandler: authenticate,
    schema: {
      body: BuildExtensionBody
    }
  })
  public async post(request: FastifyRequest<{ Body: BuildExtensionBody }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;
    const result = await this.buildService.buildNewExtension(hotelId, request.body);
    reply.sendServiceResult(result, { groups: ["player"], successCode: 201 });
  }
}
