import anyTest, { TestInterface } from "ava";
import { configureServiceTest } from "fastify-decorators/testing";
import { EntityTarget } from "typeorm";

import { SpecialRoomType } from "../../../contracts/specialroom-type.enum";
import { ServiceResultStatus } from "../../../services/service";
import { DatabaseServiceMock } from "../../../tests/database.service.mock";
import { BuildService } from "../build.service";
import { Floor } from "../entities/floor.entity";
import { Hotel } from "../entities/hotel.entity";
import { BedRoom, EmptyRoom, LobbyRoom, Room } from "../entities/room.entity";
import { HotelServiceMock } from "./hotel.service.mock";

const test = anyTest as TestInterface<{
  db: DatabaseServiceMock,
  service: BuildService,
}>;

test.before(async (t) => {
  const databaseServiceMock = new DatabaseServiceMock();
  await databaseServiceMock.create();
  t.context.db = databaseServiceMock;

  const hotelServiceMock = new HotelServiceMock(databaseServiceMock);
  await hotelServiceMock.create();

  t.context.service = configureServiceTest({
    service: BuildService,
    mocks: [
      databaseServiceMock.mock,
      hotelServiceMock.mock
    ]
  });

  await t.context.service.init();
});

test.after.always(async (t) => {
  await t.context.db.destroy();
});

test.beforeEach(async (t) => {
  const entityManager = t.context.db.connection.createEntityManager();

  const room = (position: number, entityClass: EntityTarget<Room> = EmptyRoom) => entityManager.create(entityClass, { position });

  await entityManager.save(Hotel, [{
    id: 1,
    playerId: "5d9bac23-b2a7-4ea8-ba49-83d60728c642",
    name: "First",
    money: 99999,
    level: 4,
    floors: [
      {
        level: 0,
        rooms: [room(0), room(1), room(2, LobbyRoom)]
      },
      {
        level: 1,
        rooms: [room(0), room(1), room(2, BedRoom)]
      }
    ]
  }]);
});

test.afterEach.always(async (t) => {
  await t.context.db.connection.createQueryBuilder().delete().from(Room).execute();
  await t.context.db.connection.createQueryBuilder().delete().from(Floor).execute();
  await t.context.db.connection.createQueryBuilder().delete().from(Hotel).execute();
});

test.serial("BuildService.buildNewSpecialRoom invalid hotel level", async (t) => {
  await t.context.db.connection.createQueryBuilder(Hotel, "h")
    .update({ level: 0 })
    .execute();

  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 0 })
    .getOne();

  const result = await t.context.service.buildNewSpecialRoom(1, room!, SpecialRoomType.RESTAURANT.name);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.forbidden, "operation must be forbidden");
});

test.serial("BuildService.buildNewSpecialRoom invalid hotel not enough money", async (t) => {
  await t.context.db.connection.createQueryBuilder(Hotel, "h")
    .update({ money: 0 })
    .execute();

  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 0 })
    .getOne();

  const result = await t.context.service.buildNewSpecialRoom(1, room!, SpecialRoomType.RESTAURANT.name);

  t.false(result.succeeded, "must not succeed");
  t.is(result.status, ServiceResultStatus.forbidden, "operation must be forbidden");
});

test.serial("BuildService.buildNewSpecialRoom valid", async (t) => {
  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 0 })
    .getOne();

  const result = await t.context.service.buildNewSpecialRoom(1, room!, SpecialRoomType.RESTAURANT.name);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, room!.id, "must be same id");
  t.true(result.value!.buildingEndAt! > new Date(), "building end date must be future");
  t.is(result.value!.service, SpecialRoomType.RESTAURANT, "must have correct service type");
});

test.serial("BuildService.buildNewSpecialRoom valid (room is not empty)", async (t) => {
  const room = await t.context.db.connection.createQueryBuilder(Room, "r")
    .leftJoin("floor", "f", "f.id = r.floorId")
    .where("f.hotelId = :hotelId AND f.level = :level AND r.position = :position", { hotelId: 1, level: 1, position: 2 })
    .getOne();

  const result = await t.context.service.buildNewSpecialRoom(1, room!, SpecialRoomType.RESTAURANT.name);

  t.true(result.succeeded, "must succeed");
  t.is(result.value!.id, room!.id, "must be same id");
  t.true(result.value!.buildingEndAt! > new Date(), "building end date must be future");
  t.is(result.value!.service, SpecialRoomType.RESTAURANT, "must have correct service type");
});
