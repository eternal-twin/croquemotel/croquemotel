import anyTest, { TestInterface } from "ava";
import { configureServiceTest } from "fastify-decorators/testing";
import { EntityTarget } from "typeorm";

import { DatabaseServiceMock } from "../../../tests/database.service.mock";
import { BuildService } from "../build.service";
import { Hotel } from "../entities/hotel.entity";
import { BedRoom, EmptyRoom, LobbyRoom, Room } from "../entities/room.entity";

const test = anyTest as TestInterface<{
  db: DatabaseServiceMock,
  service: BuildService,
}>;

test.before(async (t) => {
  t.context.db = new DatabaseServiceMock();
  await t.context.db.create();

  const entityManager = t.context.db.connection.createEntityManager();

  const room = (position: number, entityClass: EntityTarget<Room> = EmptyRoom) => entityManager.create(entityClass, { position });

  await entityManager.save(Hotel, [{
    id: 1,
    playerId: "5d9bac23-b2a7-4ea8-ba49-83d60728c642",
    name: "First",
    floors: [
      {
        level: 0,
        rooms: [room(0), room(1), room(2, LobbyRoom)]
      }
    ]
  }, {
    id: 2,
    playerId: "209288fd-d97d-42ac-bf42-3bc02d7d085c",
    name: "First",
    floors: [
      {
        level: 0,
        rooms: [room(0), room(1), room(2, LobbyRoom)]
      },
      {
        level: 1,
        rooms: [room(0), room(1, BedRoom), room(2, BedRoom)]
      },
      {
        level: 2,
        rooms: [room(0), room(1, BedRoom), room(2, BedRoom), room(3, BedRoom)]
      }
    ]
  }, {
    id: 3,
    playerId: "eb832790-961a-48f3-99da-1fa090754a4e",
    name: "Second",
    floors: [
      {
        level: 0,
        rooms: [room(0), room(1), room(2), room(3, LobbyRoom)]
      },
      {
        level: 1,
        rooms: [room(0, BedRoom), room(1, BedRoom), room(2, BedRoom), room(3, BedRoom)]
      },
      {
        level: 2,
        rooms: [room(0), room(1), room(2, BedRoom), room(3, BedRoom)]
      },
      {
        level: 3,
        rooms: [room(0), room(1), room(2, BedRoom), room(3, BedRoom)]
      },
      {
        level: 3,
        rooms: [room(0), room(1), room(2, BedRoom), room(3, BedRoom)]
      }
    ]
  }]);

  t.context.service = configureServiceTest({
    service: BuildService,
    mocks: [
      t.context.db.mock
    ]
  });

  await t.context.service.init();
});

test.after.always(async (t) => {
  await t.context.db.destroy();
});

test.serial("BuildService.getRoomsCount (default)", async (t) => {
  const result1 = await t.context.service.getRoomsCount(1);
  const result2 = await t.context.service.getRoomsCount(2);
  const result3 = await t.context.service.getRoomsCount(3);

  t.is(result1, 3);
  t.is(result2, 10);
  t.is(result3, 20);
});

test.serial("BuildService.getRoomsCount (LobbyRoom)", async (t) => {
  const result1 = await t.context.service.getRoomsCount(1, LobbyRoom);
  const result2 = await t.context.service.getRoomsCount(2, LobbyRoom);
  const result3 = await t.context.service.getRoomsCount(3, LobbyRoom);

  t.is(result1, 1);
  t.is(result2, 1);
  t.is(result3, 1);
});

test.serial("BuildService.getRoomsCount (BedRoom)", async (t) => {
  const result1 = await t.context.service.getRoomsCount(1, BedRoom);
  const result2 = await t.context.service.getRoomsCount(2, BedRoom);
  const result3 = await t.context.service.getRoomsCount(3, BedRoom);

  t.is(result1, 0);
  t.is(result2, 5);
  t.is(result3, 10);
});

test.serial("BuildService.getNewExtensionPrice static low price (<= 9 rooms)", async (t) => {
  const result = await t.context.service.getNewExtensionPrice(1);

  t.is(result, 0);
});

test.serial("BuildService.getNewExtensionPrice static price (<= 19 rooms)", async (t) => {
  const result = await t.context.service.getNewExtensionPrice(2);

  t.is(result, 50);
});

test.serial("BuildService.getNewExtensionPrice computed price (> 19 rooms)", async (t) => {
  const result = await t.context.service.getNewExtensionPrice(3);

  t.is(result, 100 * (20 - 6));
});
