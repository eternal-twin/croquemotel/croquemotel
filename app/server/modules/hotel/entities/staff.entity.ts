import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { IStaff } from "../../../contracts/staff.interface";
import { Hotel } from "./hotel.entity";
import { Room } from "./room.entity";

@Entity()
export class Staff implements IStaff {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  hotelId!: number;

  @ManyToOne("Hotel")
  @JoinColumn({ name: "hotelId" })
  hotel?: Hotel;

  @ManyToOne("Room", "staffs", { nullable: true })
  room?: Room;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
