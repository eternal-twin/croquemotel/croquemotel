import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { IFloor } from "../../../contracts/floor.interface";
import { Hotel } from "./hotel.entity";
import { Room } from "./room.entity";

@Entity()
export class Floor implements IFloor {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  hotelId!: number;

  @ManyToOne("Hotel")
  @JoinColumn({ name: "hotelId" })
  hotel?: Hotel;

  @OneToMany("Room", "floor", { cascade: true })
  rooms?: Room[];

  @Column()
  level!: number;

  @Column({ default: 0 })
  paint!: number;

  @Column({ default: 0 })
  wallpaper!: number;

  @Column({ default: 0 })
  wallpaperTilt!: number;

  @Column({ nullable: true })
  lowerWall?: number;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
