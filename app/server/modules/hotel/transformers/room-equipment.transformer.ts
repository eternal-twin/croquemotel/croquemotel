import { ValueTransformer } from "typeorm";

import { RoomEquipment } from "../../../contracts/room-equipment.enum";

export class RoomEquipmentTransformer implements ValueTransformer {
  to(value: RoomEquipment): any {
    return value?.name;
  }

  from(value: any): RoomEquipment {
    return RoomEquipment.from(value);
  }
}
