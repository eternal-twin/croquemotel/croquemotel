import { ValueTransformer } from "typeorm";

import { GuestType } from "../../../contracts/guest-type.enum";

export class GuestTypeTransformer implements ValueTransformer {
  to(value: GuestType): any {
    return value?.name;
  }

  from(value: any): GuestType {
    return GuestType.from(value);
  }
}
