export interface AuthToken {
  accessToken: string
  refreshToken: string
  expiresAt: number
  tokenType: "Bearer"
}
