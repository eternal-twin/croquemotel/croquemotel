import { Connection, createConnection } from "typeorm";
import { BetterSqlite3ConnectionOptions } from "typeorm/driver/better-sqlite3/BetterSqlite3ConnectionOptions";

import * as entities from "../modules/entities";
import { DatabaseService } from "../services/database.service";

export class DatabaseServiceMock {
  connection!: Connection;

  get mock() {
    return {
      provide: DatabaseService,
      useValue: { connection: this.connection }
    };
  }

  async create(): Promise<void> {
    const connectionOptions: BetterSqlite3ConnectionOptions = {
      type: "better-sqlite3",
      database: ":memory:",
      synchronize: true,
      logging: false,
      entities: [
        // resolve(dirname(fileURLToPath(import.meta.url)), "../modules/**/*.entity{.ts,.js}"),
        ...Object.values(entities)
      ]
    };

    this.connection = await createConnection(connectionOptions);
  }

  async destroy(): Promise<void> {
    await this.connection.dropDatabase();
    await this.connection.close();
  }
};
