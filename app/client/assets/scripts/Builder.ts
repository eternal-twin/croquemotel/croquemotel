import { GlowFilter } from "@pixi/filter-glow";
import { gsap } from "gsap";
import { CustomEase } from "gsap/CustomEase";
import { AnimatedSprite, Container, Filter, filters, Graphics, Sprite, Text, TextStyle, Texture, TilingSprite } from "pixi.js";

import { IFloor } from "§/contracts/floor.interface";
import { IBedRoom, ILobbyRoom, IRoom, IServiceRoom, ISpecialRoom } from "§/contracts/room.interface";
import { SpecialRoomType } from "~/../server/contracts/specialroom-type.enum";

import { GameContext } from "./GameContext";
import { Util } from "./Util";

type BuildResults = {
  fixedBackground: Container,
  hotelBackground: Container,
  hotel: Container,
};

export class Builder {
  private readonly ctx: GameContext;
  private readonly floorHeight: number = 94;
  private readonly roomWidth: number = 124;
  private readonly wallPerspectiveMax: number = 9;
  private readonly ceilingPerspectiveMax: number = 9;
  private readonly buildingsMinimumDistance: number = 130;
  private readonly treesMinimumDistance: number = 70;
  private readonly roofAssetsMinimumDistance: number = 10;
  private _clockTextStyle?: TextStyle;
  private _specialRoomTextStyle?: TextStyle;
  private _labProgressGlowFilter?: Filter;

  constructor(ctx: GameContext) {
    this.ctx = ctx;

    CustomEase.create("labFlare", "M0,0 C0,0 0.1,0.3 0.1,0.3 0.1,0.3 0.2,0.1 0.2,0.1 0.2,0.1 0.3,1 0.3,1 0.3,1 0.352,0.598 0.352,0.598 0.352,0.598 0.4,0.9 0.4,0.9 0.4,0.9 0.456,0.598 0.456,0.598 0.456,0.598 0.5,0.8 0.5,0.8 0.5,0.8 0.558,0.298 0.558,0.298 0.558,0.298 0.6,0.6 0.6,0.6 0.6,0.6 0.7,0.2 0.7,0.2 0.7,0.2 0.8,0.61 0.8,0.61 0.8,0.61 0.854,0.298 0.854,0.298 0.854,0.298 0.9,0.4 0.9,0.4 0.9,0.4 1,0 1,0");
  }

  private get clockTextStyle(): TextStyle {
    // eslint-disable-next-line no-return-assign
    return this._clockTextStyle ??= new TextStyle({
      fontFamily: this.ctx.fonts.hotelClock,
      fontSize: 18,
      fill: 0x41E2FA
    });
  }

  private get specialRoomTextStyle(): TextStyle {
    // eslint-disable-next-line no-return-assign
    return this._specialRoomTextStyle ??= new TextStyle({
      fontFamily: this.ctx.fonts.hotelRoomTitle,
      fontSize: 12,
      fill: ["#f7f400", "#fe9800"],
      fillGradientStops: [
        0.5
      ],
      dropShadow: true,
      dropShadowColor: "#000000",
      dropShadowAlpha: 0.6,
      dropShadowBlur: 1,
      dropShadowDistance: 2,
      dropShadowAngle: Math.PI / 2
    });
  }

  private get labProgressGlowFilter(): Filter {
    // eslint-disable-next-line no-return-assign
    return this._labProgressGlowFilter ??= new GlowFilter({
      color: 0xF9FF7D,
      outerStrength: 2
    });
  }

  public buildAll(): BuildResults {
    const hotel = this.buildHotel();

    this.ctx.setMap({
      width: this.roomWidth * 11,
      height: hotel.height + this.ctx.sprites.ground.height + this.floorHeight
    });

    hotel.x = this.ctx.map.width / 2 - hotel.width / 2;
    hotel.y = this.ctx.map.ground;

    const hotelBackground = this.buildHotelBackground();

    const fixedBackground = this.buildFixedBackground();

    return {
      fixedBackground,
      hotelBackground,
      hotel
    };
  }

  private buildFixedBackground(): Container {
    const container = new Container();

    const bg = new Sprite(this.ctx.sprites.bg_day);

    bg.x = 0;
    bg.y = 0;
    bg.height = this.ctx.screen.height;
    bg.width = bg.texture.width / bg.texture.height * bg.height;

    container.addChild(bg);

    return container;
  }

  private buildHotelBackground(): Container {
    const container = new Container();

    const groundPosition = this.ctx.map.ground + 4;

    // Buildings
    const buildingsStage = new Container();

    for (let i = 0, n = this.ctx.map.width / this.buildingsMinimumDistance; i < n; i++) {
      const buildingIndex = this.ctx.noiseInt(i, 1, 6);
      const building = new Sprite(this.ctx.sprites["bg_building_" + buildingIndex]);
      building.anchor.set(0.5, 1);
      building.x = i * this.buildingsMinimumDistance;
      building.y = groundPosition + 5;
      building.scale.set(this.ctx.noise(i, building.x, 0.8, 1.4));
      building.tint = 0x183E6B;
      buildingsStage.addChild(building);
    }

    buildingsStage.filters = [
      new filters.BlurFilter(5)
    ];

    container.addChild(buildingsStage);

    // Trees
    for (let i = 0, n = this.ctx.map.width / this.treesMinimumDistance; i < n; i++) {
      const treeIndex = this.ctx.noiseInt(i, 1, 3);
      const tree = new Sprite(this.ctx.sprites["tree_" + treeIndex]);
      tree.anchor.set(0.5, 1);
      tree.x = i * this.treesMinimumDistance + this.ctx.noise(i, -10, 10);
      tree.y = groundPosition;
      tree.scale.set(this.ctx.noise(i, tree.x, 0.5, 0.8));
      tree.scale.x *= this.ctx.noiseSBit(tree.scale.x * 10, treeIndex);
      container.addChild(tree);
    }

    // Ground
    const ground = new TilingSprite(this.ctx.sprites.ground, this.ctx.map.width, this.ctx.sprites.ground.height);
    ground.anchor.set(0, 1);
    ground.y = this.ctx.map.height;
    container.addChild(ground);

    return container;
  }

  private buildHotel(): Container {
    const container = new Container();

    const maxLastPos = this.ctx.data.hotel.floors!
      .map(f =>
        f.rooms!
          .map(r => r.position)
          .reduce((a, b) => Math.max(a, b))
      ).reduce((a, b) => Math.max(a, b));

    for (const floor of this.ctx.data.hotel.floors!) {
      this.buildHotelFloor({
        container,
        floor,
        maxLastPos
      });
    }

    return container;
  }

  private buildHotelFloor({ container, floor, maxLastPos }: {container: Container, floor: IFloor, maxLastPos: number}) {
    const nbRooms = floor.rooms!.length;
    const firstPos = floor.rooms![0].position;
    const lastPos = floor.rooms![floor.rooms!.length - 1].position;
    const originX = firstPos * this.roomWidth;
    const originY = -floor.level * (this.floorHeight + this.ctx.sprites.floor.height);

    const leftPerspective = (maxLastPos - firstPos) / maxLastPos * this.wallPerspectiveMax;
    const rightPerspective = lastPos / maxLastPos * this.wallPerspectiveMax;
    const topPerspective = floor.level / this.ctx.data.hotel.floors!.length * this.ceilingPerspectiveMax;

    const graphics = new Graphics();

    // Ground
    const ground = new TilingSprite(
      this.ctx.sprites.floor,
      nbRooms * this.roomWidth,
      this.ctx.sprites.floor.height
    );
    ground.position.set(originX, originY);
    container.addChild(ground);

    // Left wall
    const leftWall = new TilingSprite(
      this.ctx.sprites.wall,
      this.ctx.sprites.wall.width,
      this.floorHeight + (floor.level === 0 ? 0 : this.ctx.sprites.floor.height)
    );
    leftWall.anchor.set(0, 1);
    leftWall.x = originX;
    leftWall.y = originY + (floor.level === 0 ? 0 : this.ctx.sprites.floor.height);
    container.addChild(leftWall);

    // Left wall perspective
    graphics.beginFill(0x616F8F);
    graphics.drawRect(
      leftWall.x + this.ctx.sprites.wall.width,
      originY - this.floorHeight,
      leftPerspective,
      this.floorHeight
    );
    graphics.endFill();

    // Right wall
    const rightWall = new TilingSprite(
      this.ctx.sprites.wall,
      this.ctx.sprites.wall.width,
      this.floorHeight + this.ctx.sprites.floor.height
    );
    rightWall.anchor.set(0, 1);
    rightWall.x = (lastPos + 1) * this.roomWidth;
    rightWall.y = originY + this.ctx.sprites.floor.height;
    container.addChild(rightWall);

    // Right wall perspective
    graphics.beginFill(0x616F8F);
    graphics.drawRect(
      rightWall.x - rightPerspective,
      originY - this.floorHeight,
      rightPerspective,
      this.floorHeight
    );
    graphics.endFill();

    // Wallpaper
    const wallpaper = new TilingSprite(
      this.ctx.sprites["wallpaper_" + floor.wallpaper],
      nbRooms * this.roomWidth - this.ctx.sprites.wall.width,
      this.floorHeight
    );
    wallpaper.anchor.set(0, 1);
    wallpaper.x = originX + this.ctx.sprites.wall.width;
    wallpaper.y = originY;
    container.addChild(wallpaper);

    // Last floor roof
    if (floor.level === this.ctx.data.hotel.floors!.length - 1) {
      const floorWidth = nbRooms * this.roomWidth + this.ctx.sprites.wall.width;

      const roofLimit = originX + floorWidth - this.roofAssetsMinimumDistance;
      const roofY = originY - this.floorHeight - 5;
      let roofPrevPos = originX;

      // Roof background
      while (true) {
        const roofNextPos = roofPrevPos + this.roofAssetsMinimumDistance + this.ctx.noiseInt(roofPrevPos, floor.id!, 0, 15);
        let roofId = this.ctx.noiseInt(roofPrevPos, floor.id!, 1, 7);
        let roofSprite: Texture;
        // Look for a sprite that fit the remaining space
        do {
          roofSprite = this.ctx.sprites["roof_" + roofId];
          roofId++;
        } while (roofSprite && roofNextPos + roofSprite.width >= roofLimit);

        if (!roofSprite) {
          break;
        }

        const roof = new Sprite(roofSprite);
        roof.anchor.set(0, 1);
        roof.position.set(roofNextPos, roofY);
        container.addChild(roof);
        roofPrevPos = roof.x + roof.width;
      }

      // Roof fence
      const roofFence = new TilingSprite(
        this.ctx.sprites.fence,
        floorWidth,
        this.ctx.sprites.fence.height
      );
      roofFence.anchor.set(0, 1);
      roofFence.position.set(originX, originY - this.floorHeight);
      container.addChild(roofFence);

      // Hotel title
      container.addChild(this.buildHotelTitle(originX + (floorWidth / 2), originY - this.floorHeight - roofFence.height));

      // Intermediate roofs
    } else {
      const nextFloor = this.ctx.data.hotel.floors![floor.level + 1];
      const nextFloorFirstPos = nextFloor.rooms![0].position;
      const nextFloorLastPos = nextFloor.rooms![nextFloor.rooms!.length - 1].position;

      if (nextFloorFirstPos > firstPos) {
        const leftRoofFence = new TilingSprite(
          this.ctx.sprites.fence,
          (nextFloorFirstPos - firstPos) * this.roomWidth,
          this.ctx.sprites.fence.height
        );
        leftRoofFence.anchor.set(0, 1);
        leftRoofFence.position.set(originX, originY - this.floorHeight);
        container.addChild(leftRoofFence);
      }

      if (nextFloorLastPos < lastPos) {
        const rightRoofFence = new TilingSprite(
          this.ctx.sprites.fence,
          (lastPos - nextFloorLastPos) * this.roomWidth,
          this.ctx.sprites.fence.height
        );
        rightRoofFence.anchor.set(0, 1);
        rightRoofFence.x = (nextFloorLastPos + 1) * this.roomWidth + this.ctx.sprites.wall.width;
        rightRoofFence.y = originY - this.floorHeight;
        container.addChild(rightRoofFence);
      }
    }

    // Ceiling perspective
    graphics.beginFill(0x2E3446);
    graphics.drawPolygon([
      leftWall.x + this.ctx.sprites.wall.width,
      originY - this.floorHeight,
      rightWall.x,
      originY - this.floorHeight,
      rightWall.x - rightPerspective,
      originY - this.floorHeight + topPerspective,
      leftWall.x + this.ctx.sprites.wall.width + leftPerspective,
      originY - this.floorHeight + topPerspective
    ]);
    graphics.endFill();

    // Floor perspective (outside)
    if (floor.level > 0) {
      const prevFloor = this.ctx.data.hotel.floors![floor.level - 1];
      const prevFloorFirstPos = prevFloor.rooms![0].position;
      const prevFloorLastPos = prevFloor.rooms![prevFloor.rooms!.length - 1].position;

      const startY = originY + this.ctx.sprites.floor.height;

      graphics.beginFill(0x4D1C1A);

      if (prevFloorFirstPos > firstPos) {
        const endX = originX + (prevFloorFirstPos - firstPos) * this.roomWidth;
        graphics.drawPolygon([
          originX,
          startY,
          endX,
          startY,
          endX,
          startY + topPerspective,
          originX + leftPerspective,
          startY + topPerspective
        ]);
      }

      if (prevFloorLastPos < lastPos) {
        const startX = originX + (prevFloorLastPos - firstPos + 1) * this.roomWidth + this.ctx.sprites.wall.width;
        const endX = startX + (lastPos - prevFloorLastPos) * this.roomWidth;
        graphics.drawPolygon([
          startX,
          startY,
          endX,
          startY,
          endX - rightPerspective,
          startY + topPerspective,
          startX,
          startY + topPerspective
        ]);
      }

      graphics.endFill();
    }

    container.addChild(graphics);

    // Rooms
    let roomCount = 0;
    for (const room of floor.rooms!) {
      container.addChild(this.buildHotelRoom({
        room,
        originX: room.position * this.roomWidth + this.ctx.sprites.wall.width,
        originY,
        isFirst: roomCount === 0,
        isLast: roomCount === floor.rooms!.length - 1
      }));

      roomCount++;
    }

    // Entrance
    if (floor.level === 0) {
      container.addChild(this.buildHotelEntrance(originX, originY));
    }
  }

  private buildHotelTitle(originX: number, originY: number): Container {
    const container = new Container();

    const glowFilter = new GlowFilter({
      color: 0xE8E5D3,
      outerStrength: 6,
      distance: 5,
      quality: 0.4
    });
    container.filters = [glowFilter];

    const glowFilterTl = gsap.timeline();
    glowFilterTl.to(glowFilter, { outerStrength: 2, duration: 0.1 });
    glowFilterTl.to(glowFilter, {
      outerStrength: 6,
      duration: 0.1,
      onComplete: function hotelTitleGlowFilterComplete() {
        glowFilterTl.delay(Util.random(0.1, 5));
        glowFilterTl.restart(true);
      }
    });

    const text = new Text(this.ctx.data.hotel.name, new TextStyle({
      fontFamily: this.ctx.fonts.hotelTitle,
      fontSize: 20,
      fill: Util.newColorShade(this.ctx.data.hotel.neonColor, -15)
    }));
    text.anchor.set(0.5, 1);
    text.position.set(originX, originY);
    container.addChild(text);

    if (this.ctx.data.hotel.stars !== 0) {
      const starsWidth = this.ctx.data.hotel.stars * this.ctx.sprites.hotel_star.width;
      const starsLeft = text.x - (starsWidth / 2);
      const starsBottom = text.y - text.height;
      for (let i = 0; i < this.ctx.data.hotel.stars; i++) {
        const star = new Sprite(this.ctx.sprites.hotel_star);
        star.anchor.set(0, 1);
        star.x = starsLeft + (i * star.width);
        star.y = starsBottom;
        container.addChild(star);
      }
    }

    return container;
  }

  private buildHotelEntrance(originX: number, originY: number): Container {
    const container = new Container();

    const awningBg = new Sprite(this.ctx.sprites.awning_bg);
    awningBg.anchor.set(1, 1);
    awningBg.position.set(originX, originY);
    container.addChild(awningBg);

    const awning = new Sprite(this.ctx.sprites.awning);
    awning.anchor.set(1, 1);
    awning.position.set(originX, originY);
    container.addChild(awning);

    const hotelDoor = new Sprite(this.ctx.sprites.hotel_door);
    hotelDoor.anchor.set(0, 1);
    hotelDoor.position.set(originX, originY);
    container.addChild(hotelDoor);

    const clock = new Sprite(this.ctx.sprites.clock);
    clock.anchor.set(1, 0);
    clock.position.set(
      originX - 3,
      originY - this.floorHeight + 11
    );
    container.addChild(clock);

    const date = new Date(); // TODO: Use next customer arrival date
    const clockText = new Text(`${date.getHours()}:${Util.pad(date.getMinutes(), 2)}`, this.clockTextStyle);
    clockText.anchor.set(0.5, 0.5);
    clockText.position.set(clock.x - (clock.width / 2), clock.y + (clock.height / 2) + 1);
    container.addChild(clockText);

    return container;
  }

  private buildHotelRoom({ room, originX, originY, isFirst, isLast }: {room: IRoom, originX: number, originY: number, isFirst: boolean, isLast: boolean}): Container {
    const container = new Container();

    switch (room.type) {
      case "bed":
        this.buildHotelBedRoom(container, room as IBedRoom, originX, originY, isLast);
        break;
      case "service":
        this.buildHotelServiceRoom(container, room as IServiceRoom, originX, originY, isFirst, isLast);
        break;
      case "special":
        const specialRoom = room as ISpecialRoom;
        if (specialRoom.service !== SpecialRoomType.LAB) {
          this.buildHotelSpecialRoom(container, specialRoom, originX, originY, isFirst, isLast);
        } else {
          this.buildHotelLabRoom(container, specialRoom, originX, originY);
        }
        break;
      case "lobby":
        this.buildHotelLobbyRoom(container, room as ILobbyRoom, originX, originY);
        break;
      default:
        break;
    }

    if (room.buildingEndAt && room.buildingEndAt > new Date()) {
      this.buildHotelWorkInProgressRoom(container, room, originX, originY);
    }

    return container;
  }

  private buildHotelBedRoom(container: Container, room: IBedRoom, originX: number, originY: number, isLast: boolean) {
    const doorframe = new Sprite(this.ctx.sprites["bedroom_" + room.level]);
    doorframe.anchor.set(0, 1);
    doorframe.position.set(originX + this.ctx.noiseInt(room.id!, 10, isLast ? 10 : 25), originY);
    container.addChild(doorframe);

    const door = new Sprite(this.ctx.sprites["bedroom_door_" + (room.guest ? "1" : "0")]);
    door.anchor.set(0.5, 1);
    door.position.set(doorframe.x + (doorframe.width / 2), originY);
    container.addChild(door);

    if (room.buildingEndAt && room.buildingEndAt > new Date()) {
      const doorBuildingAsset = new Sprite(this.ctx.sprites.work_0);
      doorBuildingAsset.anchor.set(0.5, 0.5);
      doorBuildingAsset.position.set(doorframe.x + (doorframe.width / 2), originY - (doorframe.height / 2) + 3);
      container.addChild(doorBuildingAsset);
    }

    const window = new Sprite(this.ctx.sprites.bedroom_window);
    window.anchor.set(0, 1);
    window.position.set(
      doorframe.x + doorframe.width,
      originY - this.ctx.noiseInt(room.createdAt.getMilliseconds(), 20, 30)
    );
    window.angle = this.ctx.noise(room.createdAt.getTime(), -5, 5);
    container.addChild(window);
  }

  private buildHotelServiceRoom(container: Container, room: IServiceRoom, originX: number, originY: number, isFirst: boolean, isLast: boolean) {
    let sprite: Sprite;
    if (!room.serviceEndAt) {
      sprite = new Sprite(this.ctx.sprites["serviceroom_" + room.service.name]);
    } else {
      const animSprite = new AnimatedSprite(this.ctx.spritesAnimations["serviceroom_" + room.service.name]);
      animSprite.animationSpeed = 0.5;
      animSprite.play();
      sprite = animSprite;
    }

    sprite.anchor.set(0, 1);
    sprite.position.set(originX + this.ctx.noiseInt(room.id!, isFirst ? 20 : 10, isLast ? 10 : 25), originY);
    sprite.filters = [this.ctx.shadowFilter];
    container.addChild(sprite);
  }

  private buildHotelSpecialRoom(container: Container, room: ISpecialRoom, originX: number, originY: number, isFirst: boolean, isLast: boolean) {
    const bg = new Sprite(this.ctx.sprites.specialroom_bg);
    bg.anchor.set(0, 1);
    bg.position.set(originX + this.ctx.noiseInt(room.id!, isFirst ? 20 : 10, isLast ? 10 : 25), originY);
    container.addChild(bg);

    const door = new Sprite(this.ctx.sprites.specialroom);
    door.anchor.set(0, 1);
    door.position.set(bg.x, originY);
    container.addChild(door);

    if (room.service === SpecialRoomType.BIN) {
      const asset1 = new Sprite(this.ctx.sprites.specialroom_bin_1);
      asset1.anchor.set(0, 1);
      asset1.position.set(door.x + this.ctx.noiseInt(room.createdAt.getMilliseconds(), -5, 5), originY);
      asset1.filters = [this.ctx.shadowFilter];
      container.addChild(asset1);
      const asset2 = new Sprite(this.ctx.sprites.specialroom_bin_2);
      asset2.anchor.set(0, 1);
      asset2.position.set(door.x + 30 + this.ctx.noiseInt(room.createdAt.getTime(), 0, 10), originY);
      asset2.filters = [this.ctx.shadowFilter];
      container.addChild(asset2);
    } else if (room.service === SpecialRoomType.RESTAURANT || room.service === SpecialRoomType.DISCO) {
      const asset = new Sprite(this.ctx.sprites["specialroom_" + room.service.name]);
      asset.anchor.set(0.5, 1);
      asset.position.set(door.x + (door.width / 2) + (this.ctx.noiseInt(room.createdAt.getMilliseconds(), 30, 40) * this.ctx.noiseSBit(room.createdAt.getTime())), originY);
      asset.filters = [this.ctx.shadowFilter];
      container.addChild(asset);
    } else if (room.service === SpecialRoomType.FURNACE || room.service === SpecialRoomType.POOL) {
      const asset = new Sprite(this.ctx.sprites["specialroom_" + room.service.name]);
      asset.anchor.set(0.5, 1);
      asset.position.set(door.x + (door.width / 2), originY);
      container.addChild(asset);
    }

    const title = this.ctx.t(`hotel.room.${room.service.name}.title`).split(" ")[0];

    const text = new Text(title, this.specialRoomTextStyle);
    text.anchor.set(0.5, 1);
    text.position.set(bg.x + (bg.width / 2) + 1, originY - 63);
    container.addChild(text);
  }

  private buildHotelLabRoom(container: Container, room: ISpecialRoom, originX: number, originY: number) {
    const door = new Sprite(this.ctx.sprites.lab_door);
    door.anchor.set(0, 1);
    door.position.set(originX + this.ctx.noiseInt(room.id!, 10, 25), originY);
    container.addChild(door);

    const bar = new Sprite(this.ctx.sprites.lab_bar);
    bar.anchor.set(0.5, 1);
    bar.position.set(door.x + (door.width / 2), door.y - door.height - 3);
    container.addChild(bar);

    const progress = new TilingSprite(this.ctx.sprites.lab_progress, (bar.width - 2) * (room.progress ?? 0), 2);
    progress.anchor.set(0, 0);
    progress.position.set(bar.x - (bar.width / 2) + 1, bar.y - bar.height + 1);
    progress.filters = [this.labProgressGlowFilter];
    container.addChild(progress);

    let light: Sprite;
    if (room.serviceEndAt && room.serviceEndAt > new Date()) {
      const animatedLight = new AnimatedSprite(this.ctx.spritesAnimations.lab_light);
      animatedLight.animationSpeed = 0.5;
      animatedLight.play();
      light = animatedLight;

      const flare1 = new Sprite(this.ctx.sprites.lab_flare);
      flare1.position.set(door.x + 2, door.y - door.height + 1);
      const flareTl1 = gsap.timeline({ repeat: -1 });
      flareTl1.fromTo(flare1, { pixi: { alpha: 0.1 } }, { pixi: { alpha: 0.7 }, duration: 1.5, ease: "labFlare" });
      container.addChild(flare1);

      const flare2 = new Sprite(this.ctx.sprites.lab_flare);
      flare2.position.set(flare1.x + flare1.width / 1.5 - 2, flare1.y);
      const flareTl2 = gsap.timeline({ repeat: -1, delay: 0.25 });
      flareTl2.fromTo(flare2, { pixi: { alpha: 0.1 } }, { pixi: { alpha: 0.7 }, duration: 1.75, ease: "labFlare" });
      container.addChild(flare2);
    } else {
      light = new Sprite(this.ctx.sprites.lab_light);
    }

    light.anchor.set(0.5, 1);
    light.position.set(door.x + (door.width / 2), door.y - door.height - 11);
    container.addChild(light);
  }

  private buildHotelLobbyRoom(container: Container, room: ILobbyRoom, originX: number, originY: number) {
    const board = new Sprite(this.ctx.sprites.lobby_board);
    board.position.set(originX + 30, originY - board.height - 33);
    container.addChild(board);

    let i = 0;
    while (i <= room.level.level) {
      const key = new Sprite(this.ctx.sprites.lobby_key);
      key.position.set(board.x + (i * 8) + 9, board.y + 10);
      container.addChild(key);
      i++;
    }

    const clock = new Sprite(this.ctx.sprites.clock);
    clock.anchor.set(1, 0);
    clock.position.set(
      originX + this.roomWidth - this.ctx.sprites.wall.width - this.ctx.shadowFilter.distance,
      originY - this.floorHeight
    );
    clock.filters = [this.ctx.shadowFilter];
    container.addChild(clock);

    function getClockText() {
      const date = new Date();
      return `${date.getHours()} ${Util.pad(date.getMinutes(), 2)}`;
    }

    const clockText = new Text(getClockText(), this.clockTextStyle);
    clockText.anchor.set(0.5, 0.5);
    clockText.position.set(clock.x - (clock.width / 2), clock.y + (clock.height / 2) + 1);
    container.addChild(clockText);

    const clockTick = new Text(":", this.clockTextStyle.clone());
    clockTick.style.fontWeight = "bold";
    clockTick.style.fill = 0x75EEFF;
    clockTick.anchor.set(0.5, 0.5);
    clockTick.position.set(clock.x - (clock.width / 2), clock.y + (clock.height / 2) + 1);
    container.addChild(clockTick);

    function updateClockTime() {
      clockText.text = getClockText();
    }

    setTimeout(function () {
      updateClockTime();
      setInterval(updateClockTime, 60000);
    }, (60 - new Date().getSeconds()) * 1000);

    let isClockTickOn = true;
    setInterval(function updateClockTick() {
      isClockTickOn = !isClockTickOn;
      gsap.to(clockTick, { duration: 0.1, ease: "expo.inOut", pixi: { alpha: isClockTickOn ? 1 : 0.4 } });
    }, 1000);
  }

  private buildHotelWorkInProgressRoom(container: Container, room: IRoom, originX: number, originY: number) {
    const limit = this.roomWidth - this.ctx.sprites.wall.width;

    const scaffold = new Sprite(this.ctx.sprites.work_1);
    scaffold.anchor.set(0, 1);
    scaffold.position.set(originX + this.ctx.noiseInt(room.buildingEndAt!.getSeconds(), room.id!, 0, limit - scaffold.width), originY);
    container.addChild(scaffold);

    const asset = new Sprite(this.ctx.sprites["work_" + this.ctx.noiseInt(room.buildingEndAt!.getSeconds(), 2, 4)]);
    asset.anchor.set(0, 1);
    asset.position.set(originX + this.ctx.noiseInt(room.id!, room.buildingEndAt!.getSeconds(), 0, limit - asset.width), originY);
    container.addChild(asset);
  }

  private _debugRect(container: Container, { x, y, width, height, color }: { x: number, y: number, width: number, height?: number, color?: number }) {
    const graphics = new Graphics();
    graphics.beginFill(color ?? 0x00FF00);
    graphics.drawRect(0, 0, width, height ?? width);
    graphics.endFill();
    graphics.position.set(x, y);
    container.addChild(graphics);
  }
}
