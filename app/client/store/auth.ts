import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

import { $axios } from "~/utils/axios";
import { $cookies } from "~/utils/cookies";

interface ShortUser {
  id: string;
  displayName: { current: { value: string } };
}

@Module({
  name: "auth",
  stateFactory: true,
  namespaced: true
})
export default class AuthStore extends VuexModule {
  _user?: ShortUser = undefined;
  _isLoggedIn: boolean = false;

  get user() {
    return this._user;
  }

  get isLoggedIn() {
    return this._isLoggedIn;
  }

  @Mutation
  setUser(user: ShortUser) {
    this._user = user;
  }

  @Mutation
  setIsLoggedIn(isLoggedIn: boolean) {
    this._isLoggedIn = isLoggedIn;
  }

  @Mutation
  clear() {
    $cookies.remove("auth");
    this._isLoggedIn = false;
    this._user = undefined;
  }

  @Action({ rawError: true })
  async update() {
    if (this.isLoggedIn && !$cookies.get("auth")) {
      return await this.context.dispatch("logout");
    }

    if (!this.isLoggedIn && $cookies.get("auth")) {
      return await this.context.dispatch("login");
    }
  }

  @Action({ rawError: true })
  async login() {
    await this.context.dispatch("extractToken");
    await this.context.dispatch("fetchUser");
  }

  @Action({ rawError: true })
  async logout() {
    this.context.commit("clear");
    await $axios.$post("/auth/logout");
  }

  @Action({ commit: "setIsLoggedIn", rawError: true })
  extractToken() {
    return $cookies.get("auth") === "1";
  }

  @Action({ commit: "setUser", rawError: true })
  async fetchUser() {
    return await $axios.$get("/auth/me");
  }
}
