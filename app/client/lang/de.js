export default {
  welcome: "Willkommen",
  menu: {
    index: "Entdecke das Spiel",
    play: "Spielen",
    visit: "Besuche ein beliebiges Hotel",
    help: "Spielhilfe",
    office: null,
    lab: null,
    shop: null,
    friends: null
  },
  hotel: {
    room: {
      none: {
        title: "Platz...",
        desc: "Ein hübsches Plätzchen, für das du sicher schon bald etwas passendes finden wirst...",
        info: "An diesem Ort kannst du ein *neues Zimmer* bauen."
      },
      bed: {
        title: "Zimmer",
        desc: "Gäste sind ja manchmal sowas von anspruchsvoll: einige wollen einfach nicht auf dem Flurboden schlafen...",
        info: "In Zimmern kannst du deine Gäste einquartieren."
      },
      lobby: {
        title: "Rezeption",
        desc: "Ein polierter Empfangstresen mit Schlüsselregal (natürlich nur eine Imitation) wirkt professionell und schindet Eindruck bei neuen Gästen.",
        info: "Platziere deine Angestellten an der Rezeption, um die Wartezeit auf neue Gäste zu verkürzen."
      },
      lobbySlot: {
        title: "Ein freier Platz für einen weitere Rezeptionstheke.",
        desc: "Böse Zungen behaupten, dass deine Gäste hier die meiste Zeit verbringen.",
        info: "Du kannst hier eine weitere Rezeption errichten."
      },
      restaurant: {
        title: "Astro-Gastro",
        desc: "Das einzige was hier astronomisch ist, das sind die Preise.",
        info: "Gäste, die *Nahrung* über alles lieben, können hier Geld gegen Kalorien tauschen."
      },
      bin: {
        title: "Müllschlucker",
        desc: "Sein Geheimnis besteht darin, dass wir immer nur Dinge hinzufügen, aber nie wieder herausnehmen.",
        info: "Gäste, die auf *Gestank* stehen, werden sich hier wohl fühlen."
      },
      disco: {
        title: "Disko",
        desc: "Niemand versteht auch nur ein Sterbenswort davon was ein anderen erzählt. Aber irgendwie ist das auch nicht schlimm.",
        info: "Gäste mit einer Leidenschaft für *Lärm* können selbiger hier gegen Gebühr frönen."
      },
      furnace: {
        title: "Krematofen",
        desc: "Ein feuerisolierter Raum. Darin ein großer bestuhlter Ofen mit Schwingtür.",
        info: "Gäste, die von *Hitze* nicht genug bekommen, können hier ihr Geld ausgeben."
      },
      pool: {
        title: "Whirlpool",
        desc: "Das Wasser in diesem Whirlpool ist doppelt so nass wie normales Wasser! Dank HighTech ist jeder Wassertropfen mit einem feinen Ring von Wassertröpfchen ummantelt.",
        info: "Gäste mit einer Vorliebe für *Feuchtigkeit* werden sich hier prächtig amüsieren. ...natürlich gegen Bezahlung."
      },
      wash: {
        title: "Waschvollautomat A38",
        desc: "Dieses Stück HighTech kann Kleidung nicht nur waschen, sondern sie auch nach Farbe und Fleckentyp sortieren, trocknen, bügeln und zusammenlegen. ",
        info: "Damit kannst du auf Anfrage die schmutzige Wäsche deiner Gäste waschen."
      },
      shoe: {
        title: "Schuhputzer",
        desc: "Ein Apparat zum Desinfizieren, Putzen und Polieren aller Arten von Schuhen.",
        info: "Ein Gast möchte seine Schuhe reinigen lassen? Damit kannst du ihm helfen."
      },
      alcool: {
        title: "SchluckSpecht",
        desc: "Ein komplexes Geflecht von Röhren und Schläuchen versorgt die Gästezimmer mit diesem Kessel, der mit Industriealkohol und Parfüm gefüllt ist. ...auf der Karte ist von Champagner die Rede.",
        info: "Damit kannst du die Minibar von Gästen mit Hang zu starken Drinks wieder auffüllen."
      },
      fridge: {
        title: "MehrFutter-Mat",
        desc: "Die zeitgemäße Lösung für ein altes Problem: den leeren Kühlschank. Dieser Apparat liefert in Windeseile genug Nahrung, um ein ganzes Heer von Gästen zu beköstigen.",
        info: "Ermöglicht das Wiederauffüllen des Kühlschranks besonders hungriger Gäste."
      },
      lab: {
        title: "Institut der Freude",
        desc: "Der beste Weg zum besseren Verständnis deiner künftigen Gäste führt über ihr genaues Studium. Am besten scheibchenweise...",
        info: "Durch das Opfern von Gästen im \"Institut der Freude\" erwirbst du *Forschungspunkte*."
      }
    }
  }
};
