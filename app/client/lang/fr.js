export default {
  welcome: "Bienvenue",
  menu: {
    index: "Découvrir le jeu",
    play: "Jouer",
    visit: "Voir un hôtel au hasard",
    help: "Aide",
    office: "Bureau",
    lab: "Labo",
    shop: "Supermarché",
    friends: "Mes amis"
  },
  hotel: {
    room: {
      none: {
        title: "Emplacement libre pour une pièce",
        desc: "Un bel espace vide que vous ne tarderez sans doute pas à combler...",
        info: "Utilisez cet emplacement libre pour construire une *nouvelle pièce*."
      },
      bed: {
        title: "Chambre",
        desc: "Les clients sont vraiment tatillons : ils refusent de dormir à même le sol dans le couloir...",
        info: "Une chambre peut *héberger un nouveau client*."
      },
      lobby: {
        title: "Accueil",
        desc: "Un beau bureau et un panneau de clés (factices, bien entendu) pour faire pro.",
        info: "Placez des employés au bureau d'accueil pour accélérer l'arrivée des prochains clients."
      },
      lobbySlot: {
        title: "Emplacement libre pour un nouveau bureau d'accueil",
        desc: "Pour l'instant, c'est là que vos clients poireautent.",
        info: "Vous pouvez bâtir un *bureau d'accueil* supplémentaire dans cet emplacement."
      },
      restaurant: {
        title: "Restaurant astronomique",
        desc: "Il n'a d'astronomique que son prix.",
        info: "Les clients qui apprécient *la nourriture* pourront aller y dépenser leur argent."
      },
      bin: {
        title: "Vide-Ordure",
        desc: "Tout le secret réside dans le fait qu'on y jette tout, mais qu'on ne le vide jamais.",
        info: "Les clients qui apprécient *les mauvaises odeurs* pourront aller y dépenser leur argent."
      },
      disco: {
        title: "Macumbayéyé",
        desc: "Personne ne s'entend parler et cela convient à tout le monde.",
        info: "Les clients qui apprécient *le bruit* pourront aller y dépenser leur argent."
      },
      furnace: {
        title: "Crématofour",
        desc: "C'est comme un four pyrolise, mais avec des chaises dedans.",
        info: "Les clients qui apprécient *la chaleur* pourront aller y dépenser leur argent."
      },
      pool: {
        title: "Rivière sauvage",
        desc: "L'eau de cette attraction à succès a été rendue deux fois plus humide grace à l'ajout de micro-gouttes autour de chaque goutte d'eau.",
        info: "Les clients qui apprécient *l'humidité* pourront aller y dépenser leur argent."
      },
      wash: {
        title: "Savounet'",
        desc: "Un modèle performant de lave-linge qui, en plus de laver le linge, est capable de trier les couleurs, sécher, repasser et plier.",
        info: "Permet de nettoyer le linge des clients qui le demandent."
      },
      shoe: {
        title: "Lustro-pompes",
        desc: "Une cireuse capable de nettoyer et désinfecter en profondeur n'importe quelle paire de chaussures.",
        info: "Permet de nettoyer les chaussures des clients qui le demandent."
      },
      alcool: {
        title: "Poivromatique",
        desc: "Un distributeur de spiritueux connecté à de grandes cuves à essence par un réseau de tuyauterie complexe. Bien sûr, officiellement, c'est du champagne.",
        info: "Permet de fournir des boissons aux clients qui le demandent."
      },
      fridge: {
        title: "Cascadabouf",
        desc: "La solution moderne à une problèmatique vieille comme le monde : le frigo vide. Spécialement calibré pour satisfaire les besoins des monstres, il fourni, en quelques minutes, de quoi faire des apéros pendant plusieurs années.",
        info: "Permet de remplir le frigo des clients qui le demandent."
      },
      lab: {
        title: "Institut du bonheur",
        desc: "Pour mieux connaître vos futurs clients, le mieux reste encore d'étudier les actuels en les découpant en petits morceaux.",
        info: "Permet de gagner des *Points de Recherche* en sacrifiant des clients."
      }
    }
  }
};
