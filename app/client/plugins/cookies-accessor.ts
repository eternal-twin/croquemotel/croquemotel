import { Plugin } from "@nuxt/types";

import { initializeCookies } from "~/utils/cookies";

const accessor: Plugin = ({ $cookies }) => {
  initializeCookies($cookies);
};

export default accessor;
