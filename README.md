# CroqueMotel

Re-development of Motion Twin's CroqueMotel game ([hotel.muxxu.com](http://hotel.muxxu.com))

All graphic assets are licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/), the rest of the project is licensed under [AGPL-3.0-or-later](http://www.gnu.org/licenses/agpl-3.0.html).

## Tech stack

- **Frontend:** *TypeScript*, *Sass*, NuxtJS (Vue.js), PIXI
- **Backend:** *TypeScript*, Node.js, Fastify, TypeORM

## Setup

0. If you don't want to use Docker, you must install PostgreSQL
1. Install *Node.js* (>= v14): https://nodejs.org/en/download/
1. Install *Yarn*: 
    ```
    npm install -g yarn
    ```
1. Clone (or fork) this repository
1. Copy `.env.exemple` file to `.env`
1. Go to `etwin/` folder
1. Copy `etwin.toml.example` to `etwin.toml`
1. Got to `app/` folder
1. Generate a session key:
    - Linux / MacOS:
    ```bash
    ./node_modules/.bin/secure-session-gen-key > server/session-secret-key
    ```
    - Windows (**use cmd, not PowerShell**):
    ```batch
    node_modules\.bin\secure-session-gen-key > server\session-secret-key
    ```
1. Setup DB:
    - If you don't use Docker:
        1. For ETwin, follow [this guide](https://gitlab.com/eternal-twin/etwin/-/blob/master/docs/db.md)
        1. For the app, create a new database
        1. Modify `.env` and `etwin/etwin.toml` files with your DB infos
    - In any case:
        1. Start the database
        1. Go inside `etwin/` folder
        1. Install dependencies:
             ```
            yarn
            ```       
        1. Init the db:
            ```
            yarn run etwin db create
            ```

## Launching the app

### Using Docker

Inside the root folder run:
```
docker-compose up --build
```

### Legacy method

#### ETwin

1. Go to `etwin/` folder
1. Start ETwin website:
    ```
    yarn start
    ```

#### App

1. Inside `app/` folder (the first time, to install dependencies), run:
    ```
    yarn
    ```
1. Then
    - For simple launch:
        ```
        yarn build && yarn start
        ```
    - Or for live-reload:
        ```
        yarn dev
        ```
